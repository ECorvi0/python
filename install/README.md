# Use Python

## Introduction

Python is an interpreted language, that means you can run Python code very easily: you just need to have Python installed.

However, it's more convenient to use a good IDE (integrated development environment) that can help you to write code.

We will use **Spyder** or **Jupyter Notebook** (from **Anaconda**) but you can use the one you want.


## Installation of Anaconda

The program should be already installed.

If you want to installed it at home, you can look at:
* [the official explanation](https://www.anaconda.com/products/individual) (always up-to-date);
* the [Detailed installation](#detailed-installation) (more illustrated).


## How to use it?

Anaconda install Python, a lot of Python **modules** (a module is a file with Python code that you can use from another file) and several programs that can help you to code.

I will present you two programs :
* **Spyder**: which is great to make several file projects ;
* **Jupiter Notebook**: which is great to test code and visualize the output very easily.

Use the one you want (if you want to use another program there is not issue neither).

### Presentation of Spyder

1. Open Spyder:
    ![Open Spyder](img/ana_spyder_open.png)
2. You would have this:
    ![Screen of Spyder](img/ana_spyder_zones.png)

There is 3 main zones in Spyder (see screen):

* **1**: the script editor: to write scripts and run them;
* **2**: a zone to look at the variables and plots you have used (change the tab if needed);
* **3**: the IConsole: a Python console to run some line instructions.

### Présentation of Jupiter Notebook

1. Open Jupiter Notebook:
    ![Open Spyder](img/ana_jupyter_open.png)
2. You would have this:
    ![Screen of Jupyter Notebook](img/ana_jupyter_screen_1.png)

The right-windows is a console of a web server running. DO NOT CLOSE IT. But you can just ignore it.

There left-windows is the Jupyter interface. You can create a new Notebook:
![Create a new Python3 Notebook](img/ana_jupyter_screen_2.png)

This will open you a new tab where you can type Python code. You can run it by Clicking on Execute button:
![Type and run code](img/ana_jupyter_screen_3.png)


## About modules

There are a lot of Python modules that can add new functions and possibilities.

You sometimes need to install them. There is a packages manager for Python named `pip`.

### Install in Spyder

Enter in the console:

```
!pip install module-name
```

Exemple:

```
!pip install psycopg2
```

### Install in Jupyter Notebook

Enter in the notebook:

```
!pip install module-name
```

Exemple:

```
!pip install psycopg2
```

## Detailed installation

If you need more help to install Anaconda, read this!

**Download anaconda installer**

1. You need to go to [this website](https://www.anaconda.com/products/individual/);
2. Click on download button:
![Click on download button](img/ana_download_1.png)
3. Click on the link that corresponds with your system (here Windows 64 bit):
![Click on the link](img/ana_download_2.png)

**Install anaconda**

(This is made for Windows, it will change on other systems...)

1. Execute the downloaded file;
2. Follow the steps pf the installation:
3. Click on "Next":
![Click on "Next"](img/ana_install_1.png)
4. Read and accept the agreement:
![Read and accept the agreement](img/ana_install_2.png)
5. Install just for you:
![Install just for you](img/ana_install_3.png)
6. Install at the default location:
![Install at the default location](img/ana_install_4.png)
7. Register Anaconda3 as default but do not add the to the PATH:
![Register Anaconda3 as default but do not add the to the PATH](img/ana_install_5.png)
8. Wait... It is very long!:
![Wait... It is very long!](img/ana_install_6.png)
9. Click on "Next":
![Click on "Next"](img/ana_install_7.png)
10. Open nothing and click on "Finish":
![Open nothing and click on "Finish"](img/ana_install_8.png)

**Finished**

You can go back to [How to use it?](#how-to-use-it).
