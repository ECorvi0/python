# Python Sum-up

Full resources are here : [https://gitlab.com/vsasyan/python#python-pdm](https://gitlab.com/vsasyan/python#python-pdm)

## Built-in types

* **Number**: `3`, `1.42`, `-6.5`
* **Boolean**: `True` , `False`
* **String**: `'test'`, `"Hello!"`
* **List**: `[1, 2, 3]`, `['a', 'b']`, `["one", 2, 3]`
* **Tuple**: `(1, 2)`, `("a", "b", "c")`, `("one", 2, 3, "four")`
* **Dictionary**: `{'john': 42, 'abby': 21, 'william': 9}`
* **NoneType**: `None`

## Functions

* `print()` : to show data in the console;
* `input()` : to get string from the console;
* `float()` : to convert data to float;
* `int()` : to convert data to int;
* `str()` : to convert data to string;
* `len()` : to get the length of string, array, ...

## Lists

Elements are **indexed** from `0` to `n-1` :

```py
my_list = [
    'element_1',
    'element_2',
    'element_3',
]
print(my_list[0]) # print: element_1
```

## Dictionaries

It is a **key-value association** :

```py
my_dict = [
    'key_1' : 'value_1',
    'key_2' : 'value_2',
    'key_3' : 'value_3',
]
print(my_dict['key_1']) # print: value_1
```

<div style="page-break-after: always;"></div>

## Loops

There are two types of statements:

* `while`: to execute a set of work as long as **a condition is true** ;
* `for`: to **iterate on a sequence** (list, string, ...).

### While Loops

```py
import random

condition = False
while condition:
    number = random.randint(1, 10)
    condition = (number != 10)
```

Tries to find a random number equal to 10 **as long as it is not done**.

### For Loops

```py
import random

attempts = range(1, 11) # 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
for attempt_n in attempts:
    number = random.randint(1, 10)
    condition = (number != 10)
    if condition:
        print('{} was a success!'.format(attempt_n))
```

**Iterates on sequence** of attempts before stopping.

## Conditions and If statements

```py
import random

number = random.randint(1, 1000)

if number == 1000:
    print('1000!!!')
elif number > 990:
    print('Not so far... Try again!')
elif number > 900:
    print('Not so far... Still need to work.')
else:
    print('Hum...')
```
