# Functions


## Multiply

Make a function `multiply` that returns the product of the two arguments.


## A and B

Make a function that calculates and returns `a` and `b` to have `x * a + b = y`.

Tips:

* what are the arguments of the function?
* You can return two values in your function with: `return a, b`


## 111221

Write a function that returns a string that describes a string composed of numbers.

For example :

* the string `1` is composed by one "1", so it is described by the string `11`.
* the string `11` is composed by two "1" so it is described by the string `21`.
* the string `21` is composed by one "2" and one "1" so it is described by the string `1211`.
* etc.

Tips:

* what are the arguments of the function?
* you will need to process the string number by number (`for n in my_string: ...`)


## Correction


### Multiply

Parameters : `a` and `b`

Result : the multiplication of `a` by `b`

```py
def multiply(a, b):
    result = a * b
    return result

# Tests
print(multiply(2, 4))
print(multiply(2, 21))
print(multiply(0, 4))
```


### A and B

Parameters : `x` and `y`

Result : `a` and `b`

```py
def a_and_b(x, y):
    a = y // x   # Quotient of the Euclidean division
    b = y % x    # Remainder of the Euclidean division
    return a, b

# Tests
print(a_and_b(9, 2))
print(a_and_b(42, 2))
print(a_and_b(57, 4))
```


### 111221

Parameters : in_string

Result : out_string

```py
def describe(in_string):
    # Init
    out_string = "" # to store the final string
    count = 0 # to count the number of occurrence of the last letter
    last_letter = "" # to store the last letter

    # For each letter of the in_string
    for letter in in_string:
        # nex step or not?
        if letter == last_letter:
            # not, same step, just update count
            count += 1
        else:
            # save the data (if count > 0)
            if count > 0:
                out_string = f"{out_string}{count}{last_letter}".format(out_string=out_string, count=count, last_letter=last_letter)
            # new step
            last_letter = letter
            count = 1

    # Need to save the last set
    if count > 0:
        out_string = f"{out_string}{count}{last_letter}"
    return out_string


# Tests
print(describe("1"))
print(describe("11"))
print(describe("21"))
print(describe("1211"))
```
