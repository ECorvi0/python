# Lists and Dictionaries


## Create list 1

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the user 3 words,
* create an empty list,
* add the 3 words in the list,
* and then show the list (function [print](https://docs.python.org/3/library/functions.html#print)).


## Hello world! 2

Create a dictionary:

* each key is a language name;
* each associated value is "hello" in this language.

Ask the user his language (show him the possibles languages that are the keys of the dictionary) and say hello to him.

Example of dictionary:

```py
language_hello = {
    "english": "hello",
    "french": "bonjour",
    "italian": "buongiorno",
    # Add some languages...
}
```

## Corrections

## Create list 1

```py
print("Please enter 3 words :")
# Use input function to ask the user 3 words (string type)
word_1_str = input("Word #1? ")
word_2_str = input("Word #2? ")
word_3_str = input("Word #3? ")
# Create a list
my_list = [word_1_str, word_2_str, word_3_str]
# Print the list
print()
print(f"The list: {my_list}")
```


## Hello world! 2

```py
# Declare a dictionary with a lot of languages
language_hello = {
    "english": "hello",
    "french": "bonjour",
    "italian": "buongiorno",
    # Add some languages...
}
# Use input function to ask the user his language (string type)
user_language = input("What his you language (possibles values: english, french, italian)? ")
# Get the good value
if user_language == "english":
    print(language_hello["english"])
elif user_language == "french":
    print(language_hello["french"])
elif user_language == "italian":
    print(language_hello["italian"])
else:
    print("we don't do that here")
```

Latter you'll see some other functions useful with dictionary, so you can only do:

```py
# Declare a dictionary with a lot of languages
language_hello = {
    "english": "hello",
    "french": "bonjour",
    "italian": "buongiorno",
    # Add some languages...
}
# Concatenation of all the possible values separated by a comma in one string
possible_values = ", ".join(language_hello.keys())
# Use input function to ask the user his language (string type)
user_language = input(f"What his you language (possible values: {possible_values})? ")
# Test if the language exists
if user_language in language_hello.keys():
    # Print the value
    print(language_hello[user_language])
else:
    # Default value
    print("we don't do that here")
```

And, even better:

```py
# Declare a dictionary with a lot of languages
language_hello = {
    "english": "hello",
    "french": "bonjour",
    "italian": "buongiorno",
    # Add some languages...
}
# Concatenation of all the possible values separated by a comma in one string
possible_values = ", ".join(language_hello.keys())
# Use input function to ask the user his language (string type)
user_language = input(f"What his you language (possible values: {possible_values})? ")
# Print the good value (get the value associated with the key (variable user_language), if it does not exist, return the default value)
print(language_hello.get(user_language, "we don't do that here"))
```
