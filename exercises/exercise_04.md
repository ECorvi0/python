# Loops

In all theses exercices, you have to think of two things:

* the variable whose the value will change at each iteration of the loop;
* how to initialize this variable and all the others before the loop.


## Prime number

Test if an integer given by the user is a prime number.

Tips:

* A prime number is an integer greater than 1 that is not divided by any integers but 1 and itself. That means that you have to found a number that divides your number to prove that it is not a primer number. You have to test each number between 2 and your number;
* `a` divides `b` if `b % a == 0` or `int(b / a) * a == b`...
* You will need... a loop!

If you do not have idea to start, you can use this pseudocode:

* ASK user a number
* SET number_to_test = int(number)
* SET is_prime = True
* SET divisor = 1
* WHILE is_prime and divisor < number_to_test - 1
    * SET divisor = divisor + 1
    * IF divisor divides number_to_test
        * SET is_prime = False
* IF is_prime
    * SAY number_to_test is prime
* ELSE
    * SAY number_to_test is not prime


## Biggest

How to have the biggest number of an array?

Tips:

* Suppose the first number is the biggest and then try to find a biggest number iterating through the list.
* Need to have a loop...
* Need to store some data...


## Prime numbers

Make a list of all the prime numbers between 1 and 10.

Tips:

* You just need to add a *for* loop around the previous loop...


## Create list 2

Ask the user some elements until he let the input empty.

Store the elements entered by the user in a list.

At the end, show the list and the count of its elements!


## Create dictionary

Ask the user one key and one value until he let the input empty.

Store the elements entered by the user in a dictionary.

At the end, show the dictionary and the count of its keys!


## Guess the number!

Get a random number between 1 and 100.

Ask the user to enter a number, while this number is not correct, tell the user if the number his greater or smaller than the one to guess... And continue until he guess right!

To generate a random number, you need to:

1) Import the `random` package at the beginning of your script :

```py
import random
```

2) Use its function `randrange` when you want to get a random number :

```py
# I want a random number between 1 and 100 with a step of 1
int_number_to_guess = random.randrange(1, 100, 1)
```

If you do not have idea to start, you can use this pseudocode :

* SET number_to_guess = (1, 100)
* SET number_proposed = None
* WHILE number_to_guess != number_proposed
    * IF number_proposed != None
        * IF number_proposed > number_to_guess
            * SAY that number_to_guess is lower
        * ELSE
            * SAY that number_to_guess is greater
    * ASK user a number
    * CONVERT user_number (str) in number_proposed (int)
* SAY congratulation, number_to_guess was that number!


## Some Corrections

### Prime number

We have a fixed number `number_to_test`. It is not a prime number if we can find a number between 2 and its minus 1 that can divide its.

```py
# Init
number_to_test_str = input("Enter a number to test? ")
number_to_test = int(number_to_test_str) # Convert to integer
is_prime = True # From the moment we suppose that it is
divisor = 1 # Variable to store the last tested divisor

# Try to divide this number by all the numbers between 2 and him minus 1
while is_prime and divisor < number_to_test - 1:
    # We increase the value of the divisor
    divisor += 1
    # We test the new divisor
    if number_to_test % divisor == 0:
        # divisor divides the number to test, it is not a prime number
        is_prime = False
        print(f"{number_to_test} is divided by {divisor}")

# Out of the while loop, we can look at the final value of is_prime
if is_prime:
    print(f"{number_to_test} is a prime number")
else:
    print(f"{number_to_test} is not a prime number")
```

### Biggest number

We suppose the first number is the biggest and then we try to find a biggest number iterating through the list.

```py
# Init
my_list = [4, 56, 7, 98, 45, 32, 456, 87] # (can be a list entered by the user)
biggest_number = my_list[0] # I suppose the first number is the biggest

# Now I try to found a biggest number
for n in my_list:
    if n > biggest_number:
        # I found a biggest number... update biggest_number
        biggest_number = n

# Now we can print the result
print(f"{biggest_number} is the biggest number")
```

### Prime numbers

We just have to make `number_to_test` value going from 2 to 10 (1 is not a prime number).

```py
# Prime numbers

# number_to_test will take several values
for number_to_test in range(2, 11):

    is_prime = True # From the moment we suppose that it is
    divisor = 1 # Variable to store the last tested divisor

    # Try to divide this number by all the numbers between 2 and him minus 1
    while is_prime and divisor < number_to_test - 1:
        # We increase the value of the divisor
        divisor += 1
        # We test the new divisor
        if number_to_test % divisor == 0:
            # divisor divides the number to test, it is not a prime number
            is_prime = False
            print(f"{number_to_test} is divided by {divisor}")

    # Out of the while loop, we can look at the final value of is_prime
    if is_prime:
        print(f"{number_to_test} is a prime number")
    else:
        print(f"{number_to_test} is not a prime number")
```

### Create list 2

```py
# Initialisation
word_str = None    # none string
my_list = []       # empty list
# While the word is not empty
while word_str != "":
    # Use input function to ask the user a not empty word
    word_str = input("Add a word (let it empty to go out)? ")
    # Add it in the list if it is not empty
    if word_str != "":
        my_list.append(word_str)
# Print the result
print(f"The list: {my_list}")
```

### Create dictionary

```py
# Initialisation
key_str = None      # none string
value_str = None    # none string
my_dict = {}        # empty dict
# While the key are not empty
while key_str != "":
    # Use input function to ask the user a not empty key
    key_str = input("Add a key (let it empty to go out)? ")
    # If it is not empty
    if key_str != "":
        # Use input function to ask the user a value
        value_str = input("Set the corresponding value? ")
        # Add it in the dict
        my_dict[key_str] = value_str
# Print the result
print(f"The dict: {my_dict}")
```


### Guess the number

```py
import random

# Generate a number to guess
int_number_to_guess = random.randrange(1, 100, 1)
# Define a variable to store the number proposed by the user
int_number_proposed = None
# Define a number to count the number of attempts
int_nb_attempts = 1

while int_number_to_guess != int_number_proposed:

    if int_number_proposed != None:
        int_nb_attempts += 1
        if int_number_to_guess > int_number_proposed:
            advice = "The number is greater."
        else:
            advice = "The number is lighter."
        print(f"{int_number_proposed} is not the number... {advice}")

    str_entered_number = input("Try to guess the number : ")
    print()
    int_number_proposed = int(str_entered_number)

print(f"Congratulations! The number was {int_number_to_guess} ({int_nb_attempts} attempts).")
```
