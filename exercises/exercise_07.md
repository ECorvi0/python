# Pandas

## Open and look

Import `numpy`, `pandas` and `create_engine` from `sqlalchemy`.

Open the `boston_crime_incidents.csv` CSV file.

From the `rms_offense_codes.db` SQLite database, load the `rms_offense_codes` table.

Look at the dataframes (head(), info(), describe(), describe().transpose(), ...).

## Crimes data

List the days of weeks and the hours with crimes.

Make an histogram on the `HOUR` column.

Make a pie plot on the `DAY_OF_WEEK` column : add percentage, add a title and remove the "y label".

Save the pie plot.

## Crimes name

Look at the two dataframes and find the corresponding column.

Create a new dataframe by merging them. Look at the new dataframe (head(), info(), describe(), describe().transpose(), ...).

Make a pie plot on the `NAME` column (name of the crime). Keep only the first 9 crimes with the most occurrences on put all the other in a `Other` group.


## Correction

### Open and look

```py
# Importation
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sqlalchemy import create_engine

# Open crime CSV file: we have to specify the type of the OFFENSE_CODE column
crimes = pd.read_csv("boston_crime_incidents.csv", dtype={"OFFENSE_CODE": str})
# Define the engine of the SQLite database
engine = create_engine("sqlite:///rms_offense_codes.db")
# Load data of the rms_offense_codes table
codes = pd.read_sql("SELECT * FROM rms_offense_codes;", engine)

# Look at crimes
crimes.head()
crimes.describe()
codes.describe().transpose()
crimes.info()
crimes.nunique()

# Look at codes
codes.head()
codes.describe()
codes.describe().transpose()
codes.info()
codes.nunique()
```

### Crimes data

```py
# Unique values of 2 columns
crimes["DAY_OF_WEEK"].unique()
crimes["HOUR"].unique()

# Histogram of HOUR column
plt.figure()
crimes["HOUR"].hist()

# Pie plot of DAY_OF_WEEK column (with percentage, title and without ylabel)
plt.figure()
crimes["DAY_OF_WEEK"].value_counts().plot.pie(
    title="Day of Week",
    autopct="%.2f%%",
    figsize=(6, 6)
).set_ylabel("").figure.savefig("pie.png")
# and save the output (.figure.savefig("pie.png") at the end)
```

### Crimes name

```py
# Corresponding columns :
#    - crimes: OFFENSE_CODE
#    - codes: CODE

# Merge data 1: use merge function of a Dataframe and specify corresponding columns
merged_data = crimes.merge(codes, left_on="OFFENSE_CODE", right_on="CODE")
# Merge data 2: use merge function of the Pandas module and specify corresponding columns
merged_data = pd.merge(crimes, codes, left_on="OFFENSE_CODE", right_on="CODE")

# Look at merged_data
merged_data.head()
merged_data.describe()
merged_data.describe().transpose()
merged_data.info()
merged_data.nunique()

# Count different NAME
type_of_crime = merged_data["NAME"].value_counts()
# Keep the best 9 names
top = type_of_crime.iloc[:9]
# The the other names and add a new category "Other"
top.loc["Other"] = type_of_crime.iloc[9:].sum()
# Draw a pie plot
fig = top.plot.pie(
    title="Type of crime",
    autopct="%.2f%%",
    figsize=(6, 6)
).set_ylabel("")
```
