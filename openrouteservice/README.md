# Open Route Service

## Introduction

This small project is made to introduce an API. API (as Application Programming Interface), are services made to communicate: you can get information by sending simple Internet requests.

[Open Route Service](https://openrouteservice.org/) (ORS) is an API about routing.

## Account and beginning

### Account

Most of the API give you a free development account to test their service.

Create a free account on ORS: [https://openrouteservice.org/dev/#/signup](https://openrouteservice.org/dev/#/signup)

Then go to [Dashboard](https://openrouteservice.org/dev/#/home) page, create a token and copy your token:

![Create a token](img/ors_create_token.png)

The token is a string of 56 chars, you can find your active token in the [Dashboard](https://openrouteservice.org/dev/#/home):

![Active tokens](img/ors_token_list.png)


### API Playground

To catch in hand the different possibilities of the API, ORS authors have create the [API Playground](https://openrouteservice.org/dev/#/api-docs). This is an graphical interface that allows you to make requests to the API.

In this exercice, we want to:
* convert an address in a location (latitude and longitude) ;
* calculate the best journey between two locations.

Go to the [API Playground](https://openrouteservice.org/dev/#/api-docs) page...


#### Geocode

Converting an address in a location is call **geocode** an address (you can also **reverse geocode** a location in an address).

In the [API Playground](https://openrouteservice.org/dev/#/api-docs), look at the [Forward Geocode Service](https://openrouteservice.org/dev/#/api-docs/geocode/search/get):

![Geocode request](img/playground_geocode_request.png)

Do send a request, enter:
   * enter your `api_key` (token) (already done if your are logged in);
   * enter an address in the **text** parameter (ex : `5 avenue Anatole France, 75007 Paris`);
   * click **CALL ACTION**.

You can then look at the result:
* There is a map: ![Geocode map](img/playground_geocode_map.png)
* And a code: ![Geocode code](img/playground_geocode_code.png)

The map is only the representation of the code.

You should recognize list and dict data in the code, so you should be able later (in Python) to access to the coordinates information.

#### Direction

Calculate the best journey between two locations can be done by the direction functionality.

In the [API Playground](https://openrouteservice.org/dev/#/api-docs), look at the [Directions Service JSON (POST)](https://openrouteservice.org/dev/#/api-docs/v2/directions/{profile}/json/post):

![Direction request](img/playground_direction_request.png)

Do send a request, enter:
* on *HEADER* tab: enter your `Authorization` (token, already done if your are logged in);
* on *BODY* tab: enter the coordinates (ex : `[[2.36309,48.86703],[2.32738,48.86381]]`);
* on *PATH PARAMETERS* tab: enter the profile (ex : `foot-walking`);
* click **CALL ACTION**.

You can then look at the result:
* There is a map: ![Direction map](img/playground_direction_map.png)
* And a code: ![Direction code](img/playground_direction_code.png)

The map is only the representation of the code.

You should recognize list and dict data in the code, so you should be able later (in Python) to access to the coordinates information.


## A first test

Now go back to Python script editor.

The next step is to call the API from a Python script to be able to use the information in Python.

### Modules

The idea is to send an Internet request to the Navitia API to get data.

We will need to import two Python modules:

* `requests` to make Internet requests;
* `json` to manage data.

Your script must starts by:

```py
# Import modules
import requests
import json
```

The `json` module is already installed with Python, but you *may* need to install the `requests` module.

Inside Spyder or Jupyter run:

```sh
!pip install requests
```

### The Request

We need to give credential information, stored in a dictionary (remember the token!):

```py
# Header dictionary (for token)
header_dict = {
    "Authorization" : '5b3ce3597851110001cf624894bc9991e18844d58c8215f966965e3d'
}
```

Now we need an URL. They have the following pattern:

```
https://api.openrouteservice.org/{something}
```

We will use the geocoding service named `geocode` to have the coordinates of the Eiffel Tower from its address:

```py
# URL of the request
url_geocoding = 'https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf624894bc9991e18844d58c8215f966965e3d&text=5 avenue Anatole France, 75007 Paris'
```

Then we can send the request, check that the status code is ok (200 => success) and print the result:

```py
# Make the request
r = requests.get(url_geocoding, headers=header_dict)

if r.status_code == 200:
    # Get returned data of the request
    returned_data = r.text
    # Print returned_data
    print(returned_data)
else:
    print("error: status_code={}".format(r.status_code))
```

You will have the output:

```json
{"geocoding":{"version":"0.2","attribution":"https://openrouteservice.org/terms-of-service/#attribution-geocode","query":{"text":"5 avenue Anatole France, 75007 Paris","size":10,"private":false,"lang":{"name":"English","iso6391":"en","iso6393":"eng","via":"default","defaulted":true},"querySize":20,"parser":"libpostal","parsed_text":{"housenumber":"5","street":"avenue anatole france","postalcode":"75007","city":"paris"}},"engine":{"name":"Pelias","author":"Mapzen","version":"1.0"},"timestamp":1635242848168},"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[2.294597,48.858819]},"properties":{"id":"fr/countrywide:2dd6a8b49a9cee19","gid":"openaddresses:address:fr/countrywide:2dd6a8b49a9cee19","layer":"address","source":"openaddresses","source_id":"fr/countrywide:2dd6a8b49a9cee19","name":"5 Avenue Anatole France","housenumber":"5","street":"Avenue Anatole France","postalcode":"75007","confidence":1,"match_type":"exact","accuracy":"point","country":"France","country_gid":"whosonfirst:country:85633147","country_a":"FRA","macroregion":"Ile-of-France","macroregion_gid":"whosonfirst:macroregion:404227465","macroregion_a":"IF","region":"Paris","region_gid":"whosonfirst:region:85683497","region_a":"VP","localadmin":"Paris","localadmin_gid":"whosonfirst:localadmin:1159322569","locality":"Paris","locality_gid":"whosonfirst:locality:101751119","borough":"7th Arrondissement","borough_gid":"whosonfirst:borough:1158894245","neighbourhood":"Gros Caillou","neighbourhood_gid":"whosonfirst:neighbourhood:85873841","continent":"Europe","continent_gid":"whosonfirst:continent:102191581","label":"5 Avenue Anatole France, Paris, France"}}],"bbox":[2.294597,48.858819,2.294597,48.858819]}
```

This output is [JSON](https://en.wikipedia.org/wiki/JSON) formatted text. [JSON](https://en.wikipedia.org/wiki/JSON) (as JavaScript Object Notation) is a format that uses human-readable text to transmit data. You can transmit:

* lists (or arrays) represented by `[element1, element2, ...]`;
* dictionaries represented by `{"key": value}`;
* other values (string or numbers): `"one string"` or a number `42`.

So we made a request to the API, and it sent back data as JSON format.

We can modify the script to transform JSON data to Python object. Then we transform again the data in text with JSON module, but with indentation and print it.

```py
if r.status_code == 200:
    # Get returned data of the request
    returned_data = r.text
    # Transform JSON data to Python object
    data = json.loads(returned_data)
    # We Print data with indentation using json module
    print(json.dumps(data, indent=4))
else:
    print("error: status_code={}".format(r.status_code))
```

Now you should have the output:

```json
{
    "geocoding": {
        "version": "0.2",
        "attribution": "https://openrouteservice.org/terms-of-service/#attribution-geocode",
        "query": {
            "text": "5 avenue Anatole France, 75007 Paris",
            "size": 10,
            "private": false,
            "lang": {
                "name": "English",
                "iso6391": "en",
                "iso6393": "eng",
                "via": "default",
                "defaulted": true
            },
            "querySize": 20,
            "parser": "libpostal",
            "parsed_text": {
                "housenumber": "5",
                "street": "avenue anatole france",
                "postalcode": "75007",
                "city": "paris"
            }
        },
        "engine": {
            "name": "Pelias",
            "author": "Mapzen",
            "version": "1.0"
        },
        "timestamp": 1635242894267
    },
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.294597,
                    48.858819
                ]
            },
            "properties": {
                "id": "fr/countrywide:2dd6a8b49a9cee19",
                "gid": "openaddresses:address:fr/countrywide:2dd6a8b49a9cee19",
                "layer": "address",
                "source": "openaddresses",
                "source_id": "fr/countrywide:2dd6a8b49a9cee19",
                "name": "5 Avenue Anatole France",
                "housenumber": "5",
                "street": "Avenue Anatole France",
                "postalcode": "75007",
                "confidence": 1,
                "match_type": "exact",
                "accuracy": "point",
                "country": "France",
                "country_gid": "whosonfirst:country:85633147",
                "country_a": "FRA",
                "macroregion": "Ile-of-France",
                "macroregion_gid": "whosonfirst:macroregion:404227465",
                "macroregion_a": "IF",
                "region": "Paris",
                "region_gid": "whosonfirst:region:85683497",
                "region_a": "VP",
                "localadmin": "Paris",
                "localadmin_gid": "whosonfirst:localadmin:1159322569",
                "locality": "Paris",
                "locality_gid": "whosonfirst:locality:101751119",
                "borough": "7th Arrondissement",
                "borough_gid": "whosonfirst:borough:1158894245",
                "neighbourhood": "Gros Caillou",
                "neighbourhood_gid": "whosonfirst:neighbourhood:85873841",
                "continent": "Europe",
                "continent_gid": "whosonfirst:continent:102191581",
                "label": "5 Avenue Anatole France, Paris, France"
            }
        }
    ],
    "bbox": [
        2.294597,
        48.858819,
        2.294597,
        48.858819
    ]
}
```

### Process data

So the data sent by the API is a **dictionary**. Remember that a dictionary is a collection of key and value pairs.

The most important *key* is `features`. Its *value* is a **list**. As we give an address to the API, it can send back several places if the address is not enough precise...

Here is fine, there is only one feature:

```json
{
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [
            2.294597,
            48.858819
        ]
    },
    "properties": {
        "id": "fr/countrywide:2dd6a8b49a9cee19",
        "gid": "openaddresses:address:fr/countrywide:2dd6a8b49a9cee19",
        "layer": "address",
        "source": "openaddresses",
        "source_id": "fr/countrywide:2dd6a8b49a9cee19",
        "name": "5 Avenue Anatole France",
        "housenumber": "5",
        "street": "Avenue Anatole France",
        "postalcode": "75007",
        "confidence": 1,
        "match_type": "exact",
        "accuracy": "point",
        "country": "France",
        "country_gid": "whosonfirst:country:85633147",
        "country_a": "FRA",
        "macroregion": "Ile-of-France",
        "macroregion_gid": "whosonfirst:macroregion:404227465",
        "macroregion_a": "IF",
        "region": "Paris",
        "region_gid": "whosonfirst:region:85683497",
        "region_a": "VP",
        "localadmin": "Paris",
        "localadmin_gid": "whosonfirst:localadmin:1159322569",
        "locality": "Paris",
        "locality_gid": "whosonfirst:locality:101751119",
        "borough": "7th Arrondissement",
        "borough_gid": "whosonfirst:borough:1158894245",
        "neighbourhood": "Gros Caillou",
        "neighbourhood_gid": "whosonfirst:neighbourhood:85873841",
        "continent": "Europe",
        "continent_gid": "whosonfirst:continent:102191581",
        "label": "5 Avenue Anatole France, Paris, France"
    }
}
```

The feature is also a **dictionary**.

Try to get the coordinates of the feature from the `geometry` python object.

Tips:
* to get the first `feature` element it is: `data['features'][0]`;
* look at the last output: what are the other **keys** to get the coordinates?

## Exercice

Make two functions: one to geocode an address in a location (Forward Geocode Service) and one to calculate the best journey between to locations (Directions Service JSON (POST)).

### geocoding

`geocoding(address)` function will take an address as parameter and return coordinates.

Exemple:

```py
address = '5 avenue Anatole France, 75007 Paris'
coord = geocoding(address)
print(coord) # Output: {'lat': '48.858819', 'lon': '2.294597'}
```

### best journey

soon...
