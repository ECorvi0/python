# Structuring Your Code

## Make severals files

Separate your code! Make functions in separated files and keep one main file:

* one main file named `main.py`: it is the file **directly executed**;
* severals files with your custom functions;
* if needed you can have other files directly executed (to make test, load data once, etc).


## Add comments and docstring

You can make two type of comments:

* one line comment: a line starting by `#`;
* inline comment: a line of code, finishing with comment starting by `#`.


The `''' ... '''` or the `""" ... """` syntax are **not** comment (but `docstring`)!

### Comments

To explain important or complicated lines:

```py
raw_list = [
    {
        'country': 'france',
        'name': 'jean',
    },
    {
        'country': 'england',
        'name': 'john',
    },
    {
        'country': 'italy',
        'name': 'giovanni',
    }
]
pattern = "My name is {name} and I live in {country}."
# Get a new list with the pattern formatted and upper case for first letter
formatted_list = [pattern.format(name=e['name'].capitalize(), country=e['country'].capitalize()) for e in raw_list]
print(formatted_list) # Print the result
```

### docstring

`docstring` are used in Python to manage the documentation.

#### File

You can use it at the beginning of a file:

```py
# -*- coding: utf-8 -*-

"""main.py
This file runs all the process of the project, it:
    - loads of the XML data;
    - transforms XML data in geom;
    - writes Shapefile.
"""
```

#### Function

```py
def write_shapefile(geom: str, file: str) -> bool:
    """This function writes geometries in shapefile.

    Arguments:
        geom {str} -- The geometries in string
        file {str} -- The path of the file

    Returns:
        bool -- True if success
    """

    # do some stuff...
    print(geom)
    print(file)
    # have done some stuff!
    return True
```

#### Why?

You can simply get this information using the Python help function:

```py
help(print) # print() function is written with a docstring!
```

## Example

You can look at the example project in this directory:
* `main.py` is the main file ;
* `geo.py` is a module with all the geographical functions ;
* `xml_file.py` is a module with all the in/out XML functions.
