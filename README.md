# Python -- PDM

## Installation and presentation

If you need more details about the installation of Python look at [this document](install/README.md).

The [presentation](Python.pptx) ([pdf](Python.pdf)) gives you explanations and code examples to complete online courses.

## Exercices

Look at the [tutorials](tutorial#tutorial) and the [exercises](exercises#exercises).

Summary of the content:

1) [Numbers, String and Variables](exercises/exercise_01.md);
2) [Conditions](exercises/exercise_02.md);
3) [Lists and Dictionaries](exercises/exercise_03.md);
5) [Loops](exercises/exercise_04.md);
6) [Functions](exercises/exercise_05.md);
8) [Data](exercises/exercise_06.md).

Sum-Up: [online](sum-up.md) or [in PDF](sum-up.pdf)

## PDM Project

The PDM Project is here: [project-1/README.md](project/README.md) (15th November)

## Development Project

The Development Project is here: [project-2/README.md](project/README.md) (29th November)

## Resources

All the documentation of Python language is available on the official website:

[https://docs.python.org/3.8/](https://docs.python.org/3.8/)

Take care of the version in the url (especially between Python2.x and Python3.x).

### Debug and programming issues

When you have an issue: describe your issue in the search engine of your choice so you can find an answer...

[Stack Overflow](https://stackoverflow.com/) is a website of questions and answers on a wide range of topics in computer programming that can help you on many points.
